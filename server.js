var path = require('path');
var swig = require('swig');
var webpack = require('webpack');
var express = require('express');
var config = require('./webpack.config');

var app = express();
var compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

var iconsDir = path.resolve(__dirname, "./src/icons");
app.use("/icons", express.static(iconsDir));

// ### Setup Templating
app.engine("html", swig.renderFile);
app.set("views", path.resolve(__dirname, "./src/views"));
swig.setDefaults({ cache: false });

app.get('*', function(req, res) {
  res.render('landing.html');
});

app.listen(3000, function(err) {
  if (err) {
    return console.error(err);
  }

  console.log('Listening at http://localhost:3000/');
});
