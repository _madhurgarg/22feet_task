import React, { Component } from 'react'
import Stories from './stories'
import LatestNews from './latest-news'

export default class Home extends Component {
  render() {

    return (
      <div>
        <Stories />
        <h2 className='section-heading'>Latest News</h2>
        <LatestNews />
      </div>
    )
  }
}
