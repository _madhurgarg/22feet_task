import React, { Component } from 'react'
import Slider from 'react-slick'
import { latestNews, colors } from './constants'

export default class LatestNews extends Component {
  render() {
    var settings = {
      infinite: true,
      speed: 2000,
      autoplay: true,
      autoplaySpeed: 2000,
      draggable: true,
      arrows: false,
      slidesToShow: 2,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 660,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
           }
        }
      ]
    }

    return (
      <div className='latest-news'>
        <Slider {...settings}>
          {
            latestNews.map((item, i) => {
              return (
                <div className='col'>
                  <div style={{background: colors[i]}} className='news-card' key={i + 1}>
                    <h1>{ item.title }</h1>
                    <p>{ item.description }</p>
                  </div>
                </div>
              )
            })
          }
        </Slider>
      </div>
    )
  }
}
