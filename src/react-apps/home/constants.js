export const colors = ['#f46336', '#9b9b9b', '#0a8458']

export const stories = [
  {
    image: '/icons/hero-1.jpeg',
    name: 'Asian Paints',
    title: 'An Untold Story'
  },
  {
    image: '/icons/hero-2.jpeg',
    name: 'Asian Paints',
    title: 'An Untold Story'
  },
  {
    image: '/icons/hero-3.jpeg',
    name: 'Asian Paints',
    title: 'An Untold Story'
  }
]

export const latestNews = [
  {
    title: 'Vineet Gupta and Aditya Kanthy are DDV Mudra Group new flag bearers',
    description: 'Madhukar Kamath, the group CEO and MD, DDB Mudra Group, has finally decided to call it a day'
  },
  {
    title: 'Vineet Gupta and Aditya Kanthy are DDV Mudra Group new flag bearers',
    description: 'Madhukar Kamath, the group CEO and MD, DDB Mudra Group, has finally decided to call it a day'
  },
  {
    title: 'Vineet Gupta and Aditya Kanthy are DDV Mudra Group new flag bearers',
    description: 'Madhukar Kamath, the group CEO and MD, DDB Mudra Group, has finally decided to call it a day'
  }
]
