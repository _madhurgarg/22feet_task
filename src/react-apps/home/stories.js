import React, { Component } from 'react'
import Slider from 'react-slick'
import { stories } from './constants'

export default class Stories extends Component {
  constructor() {
    super()
    this.state = {
      currSlide: 1
    }
    this.beforeChange = this.beforeChange.bind(this)
    this.afterChange = this.afterChange.bind(this)
  }
  componentDidMount() {
    let prev = document.querySelector('.slick-prev')
    let next = document.querySelector('.slick-next')
    let sliderFooter = document.querySelector('.slider-footer .arrows')
    let parentNode = prev.parentNode

    parentNode.removeChild(prev)
    parentNode.removeChild(next)

    sliderFooter.appendChild(prev)
    sliderFooter.appendChild(next)
  }
  beforeChange(slide) {
    this.setState({currSlide: slide + 1})
  }
  afterChange(slide) {
    this.setState({currSlide: slide + 1})
  }
  render() {
    var settings = {
      infinite: true,
      speed: 1000,
      fade: true,
      draggable: false,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      // beforeChange: this.beforeChange,
      afterChange: this.afterChange
    }

    const { currSlide } = this.state

    return (
      <div>
        <Slider {...settings}>
          {
            stories.map((item, i) => {
              return (
                <div className='hero-slide' key={i + 1}>
                  <img src={item.image} />
                  <div className='hero-content'>
                    <h4>{item.name}</h4>
                    <h2>{item.title}</h2>
                    <a>View Case Study</a>
                  </div>
                </div>
              )
            })
          }
        </Slider>
        <div className='slider-footer'>
          <div className='pagination'>
            <span>{currSlide}</span>&nbsp;&nbsp;-&nbsp;&nbsp;<span>{stories.length}</span>
          </div>
          <div className='arrows'></div>
        </div>
      </div>
    )
  }
}
