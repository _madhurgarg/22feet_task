import React from 'react'
import { render } from 'react-dom'
import Home from './home'
import About from './about'
import './../sass/landing.scss'

console.log(location.href.split('/')[3]);

if (location.href.split('/')[3] === 'about') {
  render(<About />, document.getElementById('root'))
} else {
  render(<Home />, document.getElementById('root'))
}


document.querySelector('.hamburger ul').addEventListener('click', function (e) {
  switch (e.target.innerHTML) {
    case 'Home':
      history.pushState(null, null, '/')
      render(<Home />, document.getElementById('root'))
      break
    case 'About':
      history.pushState(null, null, '/about')
      render(<About />, document.getElementById('root'))
      break
    default:
      break
  }
})

let isSideMenuOpened = false
document.querySelector('.hamburger img').addEventListener('click', function () {
  if (!isSideMenuOpened) {
    document.querySelector('.side-menu').style.display = 'block'
    isSideMenuOpened = true
  } else {
    document.querySelector('.side-menu').style.display = 'none'
    isSideMenuOpened = false
  }
})

document.addEventListener('click', function (e) {
  if (e.target.parentNode.className === 'hamburger') {
    e.stopPropagation()
  } else {
    document.querySelector('.side-menu').style.display = 'none'
    isSideMenuOpened = false
  }
})
